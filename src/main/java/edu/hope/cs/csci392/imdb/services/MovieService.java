package edu.hope.cs.csci392.imdb.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.hope.cs.csci392.imdb.Database;
import edu.hope.cs.csci392.imdb.model.Movie;

@Path("movies")
public class MovieService {

	static class SearchRequest {
		public String title;
		public int year;
		public String primaryGenre;
		public int runningTime;
		public String runningTimeComparator;
		public String director;
	}
		
	@POST
	@Path("search")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response movieSearch(SearchRequest requestDetails) {						
		Database db = Database.getInstance();
		List<Movie> movies = db.findMovies(requestDetails.title, requestDetails.year, requestDetails.runningTimeComparator, requestDetails.runningTime, requestDetails.primaryGenre, requestDetails.director);
		return Response.ok(movies).build();
	}
}
